﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace eaile
{
    public partial class kayitOl : Form
    {
        public kayitOl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" || textBox6.Text == "" || textBox7.Text == "")
            {
                label6.Visible = true;
            }
            else
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString());
                string sorgula = "SELECT COUNT(*) FROM KullaniciAdi WHERE KullaniciAdi=@Kullanici and Sifre=@Sifre";
                con.Open();
                SqlCommand cmd = new SqlCommand(sorgula, con);

                cmd.Parameters.Add("@Kullanici", SqlDbType.NVarChar).Value = textBox1.Text;
                donenekayit = (int)cmd.ExecuteScalar();
                if (donenekayit >= 1)
                {
                    label1.Visible = true;
                    con.Close();
                }
                else
                {
                    if (textBox2.Text != textBox3.Text)
                    {
                        label2.Visible = true;
                        con.Close();
                    }
                    else
                    {
                        if (textBox4.Text != textBox5.Text)
                        {
                            label3.Visible = true;
                            con.Close();
                        }
                        else
                        {
                            if (textBox6.Text != textBox7.Text)
                            {
                                label4.Visible = true;
                                con.Close();
                            }

                            if (textBox2.Text == textBox3.Text)
                            {
                                label3.Visible = false;

                                con.Close();
                            }
                            if (textBox4.Text != textBox5.Text)
                            {
                                label6.Visible = false;

                                con.Close();
                            }
                            if (textBox6.Text == textBox7.Text)
                            {
                                label7.Visible = false;

                                con.Close();
                            }
                        }

                        if (donenekayit <= 0)
                        {
                            string Sql = "INSERT INTO KullaniciAd(Unvan,Ad,Soyad, KullaniciAd,Sifre,Meslegi) VALUES('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + textBox5.Text + "','" + textBox6.Text + "','" + textBox7.Text + "')";
                            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString());
                            SqlCommand coman = new SqlCommand(Sql, conn);

                            try
                            {
                                conn.Open();
                                coman.ExecuteNonQuery();
                                coman.Dispose();
                                conn.Close();
                                conn.Dispose();


                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
        }
        

        public int donenekayit { get; set; }
    }
}
