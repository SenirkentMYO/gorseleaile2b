﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace eaile
{
    public partial class Form1 : Form
    {
        private object k;

      
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(object k)
        {
            // TODO: Complete member initialization
            this.k = k;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            KullaniciAdi gir = new KullaniciAdi(Kul);
            gir.Show();
         
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString());
            string sorgula = "SELECT COUNT(*) FROM KullaniciAdi WHERE KullaniciAdi=@Kullanici and Sifre=@Sifre";
            con.Open();
            SqlCommand cmd = new SqlCommand(sorgula, con);
         
            cmd.Parameters.Add("@Kullanici", SqlDbType.VarChar).Value =  textBox1.Text;
            cmd.Parameters.Add("@Sifre", SqlDbType.VarChar).Value = textBox2.Text;
            int donenekayit = (int)cmd.ExecuteScalar();
            
            if (donenekayit <= 0)
            {
               MessageBox.Show("Kullanıcı Adı veya Şifre Hatalı!");
                textBox1.Text = "";
                textBox2.Text = "";
            }
            else
            {
                MessageBox.Show("GİRİŞ BAŞARILI");
                con.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            kayitOl kayit = new kayitOl();
            kayit.Show();
        }

        internal Kullanici Kul { get; set; }
    }
    }

